const functions = require('firebase-functions');


const admin = require('firebase-admin');
var serviceAccount = require("./serviceAccountKey.json");
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://wispm-3bfb4.firebaseio.com"
});

exports.updateSpent = functions.database.ref('/emg/expenses/{accountId}/{expenseId}')
.onCreate((snapshot, context) => {
	const accountId=context.params.accountId;
	const expenseId=context.params.expenseId;
	const exp = snapshot.val();
	updateTotal(admin.database().ref('/emg/accounts/'+accountId), exp.cantitate*exp.pret,exp.categorie);
});

function updateTotal(ref,amount,categorie){

	ref.transaction(function(account) {
		if (account) {

				for(i=0;i<account.categories.length;i++){
					if(account.categories[i].id===categorie){
						if (account.categories[i].spent) {
	  	 					account.categories[i].spent=account.categories[i].spent+amount;
  	 					} else {
  	 						account.categories[i].spent=amount;
  	 					}
					}
  	 			}

  	 			if (account.spent) {
  	 				account.spent=account.spent+amount;
  	 			} else {
  	 				account.spent=amount;
  	 			}
  	 		}
  	 		return account;
  	 	});
}  



exports.pushNotif = functions.database.ref('/emg/accounts/{pushId}')
.onUpdate((snapshot, context) => {
 	const account = snapshot.after.val();
    const spent=account.spent;
    const limit = account.limit;
    if(spent&&limit&&spent>limit){
     	let notification= {
			body: "Spent limit exceded for account "+account.name,
			title: "Spent limit exceded"
		}
		console.log(account.owners)
		if(account.owners){
			console.log("account.owners")
			let keys = Object.keys(account.owners);
			for(i=0;i<keys.length;i++){
				if(account.owners[keys[i]]){
					sendNotification(keys[i],notification);
				}
				/*for(i=0;i<account.categories.length;i++){
 					if(account.categories[i].limit&&account.categories[i].limit<account.categories[i].spent ){
						sendNotification(keys[i],notification);
					}
				} */
			}
		}
      }
      
	});
function sendNotification(userId, notification){
	return admin.database().ref("/emg/users/" + userId).once('value').then(snap => {
		const token = snap.child("push").val();
		console.log("userId: ", userId);
		console.log("token: ", token);
		if(token){
			const payload = {
				notification:notification
			}

			return admin.messaging().sendToDevice(token, payload)
			.then(function(response) {
				console.log("message sent:", response);
				return null;
			})
			.catch(function(error) {
				console.log("err:", error);
				return null;
			});
		}
		return null;
	});
 
}





