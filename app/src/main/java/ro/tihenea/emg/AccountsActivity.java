package ro.tihenea.emg;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import ro.tihenea.emg.Fragments.AccountsFragment;
import ro.tihenea.emg.Fragments.dummy.DummyContent;
import ro.tihenea.emg.model.Account;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

public class AccountsActivity extends AppCompatActivity implements AccountsFragment.OnListFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FragmentTransaction var10000 = this.getSupportFragmentManager().beginTransaction();
        AccountsFragment accountsActivity = new AccountsFragment();
        var10000.replace(R.id.content_frame, (Fragment) accountsActivity);
        var10000.commit();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onListFragmentInteraction(Account item) {
        System.out.println(item.getName());
        Intent intent = new Intent(this, AccountsOwnersActivity.class);
        intent.putExtra(Constants.NAME, item.getName());
        intent.putExtra(Constants.ID, item.getId());
        ArrayList<String> owners = (ArrayList<String>) item.getOwners().entrySet().stream().filter(Map.Entry::getValue).map(Map.Entry::getKey).collect(Collectors.toList());
        intent.putExtra(Constants.OWNERS, owners);
        startActivity(intent);
    }
}
