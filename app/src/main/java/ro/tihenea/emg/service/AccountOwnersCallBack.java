package ro.tihenea.emg.service;

import ro.tihenea.emg.model.Product;

import java.util.List;

public interface AccountOwnersCallBack {
    void renderOwners(List<String> owners);
}
