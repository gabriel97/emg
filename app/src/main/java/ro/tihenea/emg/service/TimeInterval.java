package ro.tihenea.emg.service;

import android.app.FragmentManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import ro.tihenea.emg.IntervalType;
import ro.tihenea.emg.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static java.util.Calendar.*;
import static ro.tihenea.emg.IntervalType.DAY;
import static ro.tihenea.emg.IntervalType.WEEK;

public class TimeInterval implements DatePickerDialog.OnDateSetListener {
    private FragmentManager fragmentManager;
    private IntervalType intervalType = DAY;
    private Calendar intervalStart;
    private Calendar intervalEnd;
    private AlertDialog dialog;
    private View dialogView;
    private List<TimeIntervalCallBack> timeIntervalCallBacks = new ArrayList<>();
    private TextView timeSelect;


    public TimeInterval(Context context) {
        LayoutInflater factory = LayoutInflater.from(context);

        dialogView = factory.inflate(R.layout.time_select, null);
        dialog = new AlertDialog.Builder(context).create();
        dialog.setView(dialogView);
        intervalStart = Calendar.getInstance();
        intervalStart.set(Calendar.HOUR_OF_DAY, 0);
        intervalStart.set(Calendar.MINUTE, 0);
        intervalStart.set(Calendar.SECOND, 0);
        intervalStart.set(Calendar.MILLISECOND, 0);

        intervalEnd = Calendar.getInstance();
        intervalEnd.set(Calendar.HOUR_OF_DAY, 23);
        intervalEnd.set(Calendar.MINUTE, 59);
        intervalEnd.set(Calendar.SECOND, 59);
        intervalEnd.set(Calendar.MILLISECOND, 999);

    }

    public void registerCallback(TimeIntervalCallBack timeIntervalCallBack) {
        if (timeIntervalCallBack != null) {
            timeIntervalCallBacks.add(timeIntervalCallBack);
            timeIntervalCallBack.onTimeIntervalChange(intervalStart, intervalEnd);

        }
    }

    public void nextTimeInterval() {
        callBack();

        switch (intervalType) {
            case DAY:
                intervalEnd.add(Calendar.DAY_OF_YEAR, 1);
                intervalStart.add(Calendar.DAY_OF_YEAR, 1);
                break;
            case WEEK:
                intervalEnd.add(Calendar.WEEK_OF_YEAR, 1);
                intervalStart.add(Calendar.WEEK_OF_YEAR, 1);
                break;
            case MONTH:
                intervalEnd.add(MONTH, 1);
                intervalStart.add(MONTH, 1);
                break;
        }
        callBack();
    }

    private void callBack() {
        SimpleDateFormat format1 = new SimpleDateFormat("MM-dd");
        String end;
        String start = format1.format(intervalStart.getTime());
        end = format1.format(intervalEnd.getTime());
        timeSelect.setText(start + " : " + end);
        for (TimeIntervalCallBack timeIntervalCallBack : timeIntervalCallBacks) {
            timeIntervalCallBack.onTimeIntervalChange(intervalStart, intervalEnd);
        }
    }

    public void previousTimeInterval() {
        switch (intervalType) {
            case DAY:
                intervalEnd.add(Calendar.DAY_OF_YEAR, -1);
                intervalStart.add(Calendar.DAY_OF_YEAR, -1);
                break;
            case WEEK:
                intervalEnd.add(Calendar.WEEK_OF_YEAR, -1);
                intervalStart.add(Calendar.WEEK_OF_YEAR, -1);
                break;
            case MONTH:
                intervalEnd.add(MONTH, -1);
                intervalStart.add(MONTH, -1);
                break;
        }
        callBack();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        intervalStart.set(Calendar.YEAR, year);
        intervalStart.set(MONTH, monthOfYear);
        intervalStart.set(DAY_OF_MONTH, dayOfMonth);
        intervalEnd.set(Calendar.YEAR, year);
        intervalEnd.set(MONTH, monthOfYear);
        intervalEnd.set(DAY_OF_MONTH, dayOfMonth);
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        String formatter = format1.format(intervalEnd.getTime());
        timeSelect.setText(formatter);
        intervalType = DAY;
        callBack();
    }

    public void showTimeSelect(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
        dialogView.findViewById(R.id.time_select_all_time).setOnClickListener(v -> {
            selectTime(R.id.time_select_all_time, dialog, v);
        });

        dialogView.findViewById(R.id.toolbar_select_day).setOnClickListener(v -> {
            intervalType = DAY;
            selectTime(R.id.toolbar_select_day, dialog, v);
        });

        dialogView.findViewById(R.id.time_select_week).setOnClickListener(v -> {
            intervalType = WEEK;
            selectTime(R.id.time_select_week, dialog, v);
        });

        dialogView.findViewById(R.id.time_select_today).setOnClickListener(v -> {
            selectTime(R.id.time_select_today, dialog, v);
        });

        dialogView.findViewById(R.id.time_select_year).setOnClickListener(v -> {
            selectTime(R.id.time_select_year, dialog, v);
        });
        dialogView.findViewById(R.id.time_select_month).setOnClickListener(v -> {
            intervalType = IntervalType.MONTH;
            selectTime(R.id.time_select_month, dialog, v);
        });

        dialog.show();
    }

    private void selectTime(int time_select, AlertDialog dialog, View view) {


        switch (time_select) {
            case R.id.time_select_all_time:
                intervalStart = Calendar.getInstance();
                intervalStart.set(YEAR, 1900);
                intervalStart.set(Calendar.HOUR_OF_DAY, 0);
                intervalStart.set(Calendar.MINUTE, 0);
                intervalStart.set(Calendar.SECOND, 0);
                intervalStart.set(Calendar.MILLISECOND, 0);

                intervalEnd = Calendar.getInstance();
                intervalEnd.set(Calendar.HOUR_OF_DAY, 23);
                intervalEnd.set(Calendar.MINUTE, 59);
                intervalEnd.set(Calendar.SECOND, 59);
                intervalEnd.set(Calendar.MILLISECOND, 999);
                callBack();
            case R.id.toolbar_select_day:
                startDatePicker();
                break;
            case R.id.time_select_week:
                intervalStart.set(Calendar.DAY_OF_WEEK, intervalStart.getFirstDayOfWeek());
                intervalEnd.set(DAY_OF_WEEK, intervalStart.getActualMaximum(DAY_OF_WEEK));

                callBack();
                break;
            case R.id.time_select_today:
                intervalStart = Calendar.getInstance();
                intervalStart.set(Calendar.HOUR_OF_DAY, 0);
                intervalStart.set(Calendar.MINUTE, 0);
                intervalStart.set(Calendar.SECOND, 0);
                intervalStart.set(Calendar.MILLISECOND, 0);

                intervalEnd = Calendar.getInstance();
                intervalEnd.set(Calendar.HOUR_OF_DAY, 23);
                intervalEnd.set(Calendar.MINUTE, 59);
                intervalEnd.set(Calendar.SECOND, 59);
                intervalEnd.set(Calendar.MILLISECOND, 999);
                callBack();
                break;
            case R.id.time_select_year:
                intervalStart.set(MONTH, JANUARY);
                intervalStart.set(DAY_OF_MONTH, 1);
                intervalEnd.set(MONTH, DECEMBER);
                intervalEnd.set(DAY_OF_MONTH, 31);

                callBack();
                break;
            case R.id.time_select_month:
                intervalStart.set(DAY_OF_MONTH, 1);
                intervalEnd.set(DAY_OF_MONTH, intervalStart.getActualMaximum(DAY_OF_MONTH));
                callBack();
                break;
        }

        dialog.dismiss();
    }

    private void startDatePicker() {

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, intervalStart.get(Calendar.YEAR),
                intervalStart.get(MONTH), intervalStart.get(DAY_OF_MONTH));
        datePickerDialog.setThemeDark(false);
        datePickerDialog.showYearPickerFirst(false);
        datePickerDialog.setTitle("Date Picker");

        datePickerDialog.show(fragmentManager, "DatePickerDialog");

    }

    public void selectText(TextView timeSelect) {
        this.timeSelect = timeSelect;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        String formatter = format1.format(intervalEnd.getTime());
        timeSelect.setText(formatter);
    }
}
