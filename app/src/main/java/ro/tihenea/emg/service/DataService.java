package ro.tihenea.emg.service;

import android.util.Log;
import androidx.annotation.NonNull;
import com.google.firebase.database.*;
import org.jetbrains.annotations.Nullable;
import ro.tihenea.emg.LoginActivity;
import ro.tihenea.emg.model.*;

import java.util.*;
import java.util.stream.Collectors;

public class DataService {

    private static DataService dataService = new DataService();
    private final List<Product> products;
    private final List<Account> accounts = new ArrayList<>();
    private Calendar start;
    private Calendar end;
    private List<ProductCallBack> productCallBacks = new ArrayList<>();

    private DataService() {
        products = new ArrayList<>();
        initStartEnd();
    }

    public static DataService getDataService() {
        return dataService;
    }

    public void initData(User user) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();


        if (user != null) {
            for (Map.Entry<String, Boolean> accountEntry : user.getAccounts().entrySet()) {
                System.out.println("account" + accountEntry.getKey());
                DatabaseReference myRef = database.getReference("emg").child("accounts").child(accountEntry.getKey());
                ValueEventListener postListener = new ValueEventListener() {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        Account account = dataSnapshot.getValue(Account.class);
                        if (!accounts.contains(account)) {
                            if (!accounts.contains(account)) accounts.add(account);
                        }
                        if (account.getId() != null) addDataListenerForAccount(database, account.getId());

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.w("rsf", "loadPost:onCancelled", databaseError.toException());

                    }
                };
                myRef.addValueEventListener(postListener);

            }
        }
    }

    private void addDataListenerForAccount(FirebaseDatabase database, String account) {
        System.out.println("expenses" + account);
        DatabaseReference myRef = database.getReference("emg").child("expenses").child(account);

        ValueEventListener postListener = new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataSnapshot.getChildren().forEach(product -> {
                    ProductDB productDB = product.getValue(ProductDB.class);
                    Product product1 = new Product(productDB);
                    product1.setAccount(account);
                    if (!products.contains(product1)) products.add(product1);
                });
                callBackNewInterval();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("rsf", "loadPost:onCancelled", databaseError.toException());

            }
        };
        myRef.addValueEventListener(postListener);
    }

    private void initStartEnd() {
        start = Calendar.getInstance();
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);

        end = Calendar.getInstance();
        end.set(Calendar.HOUR_OF_DAY, 23);
        end.set(Calendar.MINUTE, 59);
        end.set(Calendar.SECOND, 59);
        end.set(Calendar.MILLISECOND, 999);

    }

    public void addProductListener(ProductCallBack productCallBack) {
        productCallBacks.add(productCallBack);
        callBackNewInterval();
    }

    public void setTimeInterval(Calendar start, Calendar end) {
        this.start = start;
        this.end = end;
        callBackNewInterval();
    }

    private void callBackNewInterval() {
        System.out.println(start.getTimeInMillis());
        productCallBacks.forEach(callBack -> callBack.renderProducts(
                products.stream().filter(product -> start.getTimeInMillis() / 1000 <= product.getTimestamp() && end.getTimeInMillis() / 1000 >= product.getTimestamp()).collect(Collectors.toList())));

    }

//    public List<Account> getAccounts() {

   /*     Query query = myRef.orderByChild("categorie").equalTo("asd");//limitToFirst(4);

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                System.out.println(dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("ouyt", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        query.addValueEventListener(postListener);*/
    //myRef.push().setValue(new Product("Utilitati", "electricitate", 34.0, System.currentTimeMillis(), 1));
    //myRef.push().setValue(new Product("Divertisment", "teatru", 20.0, System.currentTimeMillis(), 1));

//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = database.getReference("produse").child("produse");
//
//        ValueEventListener postListener = new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                System.out.println(dataSnapshot.getValue())
//                ITEMS.add(dataSnapshot.getValue(Product:: class.java)as Product)
//                adapter = MyHistoryRecyclerViewAdapter(ITEMS, listener)
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//                Log.w(TAG, "loadPost:onCancelled", databaseError.toException())
//
//            }
//
//
//        };
//        myRef.addValueEventListener(postListener)
//    }
//    }

    private void firstSetup(DatabaseReference myRef) {
        String uid = LoginActivity.getUser().getUid();

        ro.tihenea.emg.model.User user = new ro.tihenea.emg.model.User();
        user.setName(LoginActivity.getUser().getDisplayName());
        Account account = new Account();
        account.setName("Test");
        HashMap<String, Boolean> owners = new HashMap<>();
        owners.put(uid, true);
        account.setOwners(owners);
        account.setCategories(new LinkedList<>());
        account.getCategories().add(new Category("1234", "Mancare"));
        DatabaseReference newAccount = myRef.child("accounts").push();
        String accountId = newAccount.getKey();
        Expense expense = new Expense();
        newAccount.setValue(account);
        user.setAccounts(new HashMap<>());
        user.getAccounts().put(accountId, true);
        myRef.child("users").child(uid).setValue(user);
        myRef.child("expenses").child(accountId).push().setValue(expense);
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void addAccountOwnersCallBack(AccountOwnersCallBack asd, String accountId) {
        List<String> list = new LinkedList<>();
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference myRef = database.getReference("emg").child("accounts").child(accountId).child("owners");

        ValueEventListener postListener = new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataSnapshot.getChildren().forEach(account -> {
                    getUserNameForAccount(list, account.getKey(), asd);
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("rsf", "loadPost:onCancelled", databaseError.toException());

            }
        };
        myRef.addValueEventListener(postListener);
    }

    private void getUserNameForAccount(List<String> list, String account, AccountOwnersCallBack asd) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference myRef = database.getReference("emg").child("users").child(account).child("name");

        ValueEventListener postListener = new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                list.add(dataSnapshot.getValue(String.class));
                asd.renderOwners(list);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("rsf", "loadPost:onCancelled", databaseError.toException());
            }
        };
        myRef.addValueEventListener(postListener);
    }

    public void addUserToAccount(String userId, String accountId) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("emg");
        myRef.child("users").child(userId).child("accounts").child(accountId).setValue(true);
        myRef.child("accounts").child(accountId).child("owners").child(userId).setValue(true);
    }

    @Nullable
    public CharSequence getCategoryName(@Nullable String account, @Nullable String categorie) {
        Optional<List<Category>> optionalCategories = getAccounts().stream().filter(x -> x.getId().equals(account)).findFirst().map(Account::getCategories);
        if (optionalCategories.isPresent()) {
            Optional<Category> optionalCategory = optionalCategories.get().stream().filter(y -> y.getId().equals(categorie)).findFirst();
            if (optionalCategory.isPresent()) {
                return optionalCategory.get().getName();
            }
        }
        return "";
    }
}
