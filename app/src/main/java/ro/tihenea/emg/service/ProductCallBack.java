package ro.tihenea.emg.service;

import ro.tihenea.emg.model.Product;

import java.util.List;

public interface ProductCallBack {
    void renderProducts(List<Product> products);
}
