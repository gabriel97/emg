package ro.tihenea.emg.service;

import java.util.Calendar;

public interface TimeIntervalCallBack {

    void onTimeIntervalChange(Calendar start, Calendar end);
}
