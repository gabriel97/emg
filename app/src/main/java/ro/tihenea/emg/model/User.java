package ro.tihenea.emg.model;

import java.util.HashMap;
import java.util.List;

public class User {
    String id;
    String name;
    HashMap<String, Boolean> accounts;
    HashMap<String, Boolean> friends;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, Boolean> getAccounts() {
        return accounts;
    }

    public void setAccounts(HashMap<String, Boolean> accounts) {
        this.accounts = accounts;
    }

    public HashMap<String, Boolean> getFriends() {
        return friends;
    }

    public void setFriends(HashMap<String, Boolean> friends) {
        this.friends = friends;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
