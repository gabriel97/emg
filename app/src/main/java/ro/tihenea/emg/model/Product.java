package ro.tihenea.emg.model;

import java.util.Objects;

public class Product {
    private String categorie;
    private String produs;
    private Float pret;
    private long timestamp;
    private int cantitate;
    private String account;

    public Product(ProductDB productDB) {

        this.categorie = productDB.getCategorie();
        this.produs = productDB.getProdus();
        this.pret = productDB.getPret();
        this.timestamp = productDB.getTimestamp();
        this.cantitate = productDB.getCantitate();
    }


    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getProdus() {
        return produs;
    }

    public void setProdus(String produs) {
        this.produs = produs;
    }

    public Float getPret() {
        return pret;
    }

    public void setPret(Float pret) {
        this.pret = pret;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return timestamp == product.timestamp &&
                cantitate == product.cantitate &&
                Objects.equals(categorie, product.categorie) &&
                Objects.equals(produs, product.produs) &&
                Objects.equals(pret, product.pret) &&
                Objects.equals(account, product.account);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categorie, produs, pret, timestamp, cantitate, account);
    }
}
