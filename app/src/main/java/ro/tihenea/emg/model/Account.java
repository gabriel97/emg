package ro.tihenea.emg.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Account implements Serializable {
    Map<String, Boolean> owners;
    String name;
    List<Category> categories;
    private String id;

    public Map<String, Boolean> getOwners() {
        return owners;
    }

    public void setOwners(Map<String, Boolean> owners) {
        this.owners = owners;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(owners, account.owners) &&
                Objects.equals(name, account.name) &&
                Objects.equals(categories, account.categories) &&
                Objects.equals(id, account.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owners, name, categories, id);
    }
}
