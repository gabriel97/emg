package ro.tihenea.emg.model;

public class ProductHistory {
    String name;
    String unit;
    Double price;
    Double noUnit;
    Long id;

    public ProductHistory(String name, String unit, Double price, Double noUnit) {
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.noUnit = noUnit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getNoUnit() {
        return noUnit;
    }

    public void setNoUnit(Double noUnit) {
        this.noUnit = noUnit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
