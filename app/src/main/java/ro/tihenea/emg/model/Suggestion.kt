package ro.tihenea.emg.model

import java.util.*


class Suggestion(


    var recomended: StoreProduct,
    var oldChoice: StoreProduct
) {
}

class StoreProduct(
    var store: Store,
    var item: Item,
    var pricePerUnit: Double, var addedAt: String,
    var id: Long
) {

}

class Item(
    var name: String,
    var unit: String,
    var id: Long,
    var stores: List<Object>
)

class Store(
    var storeName: String,
    var storeLocation: String,
    var storeId: Long,
    var items: List<Object>
)

