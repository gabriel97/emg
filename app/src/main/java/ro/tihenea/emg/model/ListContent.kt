package ro.tihenea.emg.model

import java.util.ArrayList
import java.util.HashMap

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 *
 * TODO: Replace all uses of this class before publishing your app.
 */
object ListContent {

    /**
     * An array of sample (dummy) items.
     */
    val ITEMS: MutableList<ListItem> = ArrayList()

    /**
     * A map of sample (dummy) items, by ID.
     */

    private val COUNT = 25

    init {
        // Add some sample items.
        // for (i in 1..COUNT) {
        addItem(
            ListItem(
                "Mega-Image",
                "12-06-2019 8:24",
                "32.24"
            )
        )
        addItem(
            ListItem(
                "Mega-Image",
                "21-05-2019 23:10",
                "5.17"
            )
        )
        addItem(
            ListItem(
                "Mega-Image",
                "21-05-2019 15:23",
                "12.24"
            )
        )
        addItem(
            ListItem(
                "Mega-Image",
                "20-05-2019 10:34",
                "22.24"
            )
        )
//        }
    }

    private fun addItem(item: ListItem) {
        ITEMS.add(item)
    }


    data class ListItem(val store: String, val date: String, val price: String) {


    }
}
