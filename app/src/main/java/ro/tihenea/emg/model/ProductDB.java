package ro.tihenea.emg.model;

public class ProductDB {
    private String categorie;
    private String produs;
    private Float pret;
    private long timestamp;
    private int cantitate;

    public ProductDB() {
    }

    public ProductDB(String categorie, String produs, Float pret, long timestamp, int cantitate) {
        this.categorie = categorie;
        this.produs = produs;
        this.pret = pret;
        this.timestamp = timestamp;
        this.cantitate = cantitate;
    }


    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getProdus() {
        return produs;
    }

    public void setProdus(String produs) {
        this.produs = produs;
    }

    public Float getPret() {
        return pret;
    }

    public void setPret(Float pret) {
        this.pret = pret;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }
}
