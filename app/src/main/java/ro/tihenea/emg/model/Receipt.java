package ro.tihenea.emg.model;

import java.util.List;
import java.util.Objects;

public class Receipt {
    private String storeName;
    private String storeLocation;
    private Double totalPrice;
    private String store;
    private List<Product> products;
    private String addedAt;
    private Long id;

    public Receipt() {
    }

    public Receipt(String storeName, String storeLocation, Double totalPrice, String store, List<Product> products, String addedAt, Long id) {
        this.storeName = storeName;
        this.storeLocation = storeLocation;
        this.totalPrice = totalPrice;
        this.store = store;
        this.products = products;
        this.addedAt = addedAt;
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Receipt receipt = (Receipt) o;
        return Objects.equals(storeName, receipt.storeName) &&
                Objects.equals(storeLocation, receipt.storeLocation) &&
                Objects.equals(totalPrice, receipt.totalPrice) &&
                Objects.equals(store, receipt.store) &&
                Objects.equals(products, receipt.products) &&
                Objects.equals(addedAt, receipt.addedAt) &&
                Objects.equals(id, receipt.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(storeName, storeLocation, totalPrice, store, products, addedAt, id);
    }

    @Override
    public String toString() {
        return "Receipt{" +
                "storeName='" + storeName + '\'' +
                ", storeLocation='" + storeLocation + '\'' +
                ", totalPrice=" + totalPrice +
                ", store='" + store + '\'' +
                ", products=" + products +
                ", addedAt='" + addedAt + '\'' +
                ", id=" + id +
                '}';
    }

    public String getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(String storeLocation) {
        this.storeLocation = storeLocation;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getAddedAt() {
        return addedAt;
    }

    public void setAddedAt(String addedAt) {
        this.addedAt = addedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
}
