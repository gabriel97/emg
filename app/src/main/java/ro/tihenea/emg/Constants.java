package ro.tihenea.emg;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import org.jetbrains.annotations.Nullable;

public class Constants {
    public static final String URL="http://139.59.208.125:8080";
    public static final String BUY_LIST="/buy";
    public static final String SUGGESTIONS="/suggestions";
    public static final String RECEIPTS ="/receipts";
    public static final String PRODUCTS ="/products";
    public static final String OWNERS = "owners";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String ACCOUNT_ID = "accountId";
    @Nullable
    public static GoogleSignInClient googleSignInClient;

}
