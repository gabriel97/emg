package ro.tihenea.emg;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import ro.tihenea.emg.Fragments.AddOwner;
import ro.tihenea.emg.service.AccountOwnersCallBack;

import java.util.List;

public class AccountsOwnersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts_owners);
        Intent intent = getIntent();
        String name = intent.getStringExtra(Constants.NAME);
        setTitle(name);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        String accountId = intent.getStringExtra(Constants.ID);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), AddOwner.class);
                i.putExtra(Constants.ACCOUNT_ID, accountId);
                startActivity(i);
            }
        });
        diplayOwners(accountId);
    }

    private void diplayOwners(String accountId) {

        RecyclerView mResultList = (RecyclerView) findViewById(R.id.list);
        mResultList.setHasFixedSize(true);
        mResultList.setLayoutManager(new LinearLayoutManager(this));

        AccountOwnersCallBack asd = owners -> mResultList.setAdapter(new AOA(owners));
        MainActivity.dataService.addAccountOwnersCallBack(asd, accountId);

    }

    private class AOA extends RecyclerView.Adapter<AOA.ViewHolder> {
        private final List<String> mValues;

        public AOA(List<String> owners) {
            mValues = owners;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_accounts, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            holder.mItem = mValues.get(position);
            holder.mIdView.setText(mValues.get(position));
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public String mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.item_number);
            }
        }
    }
}
