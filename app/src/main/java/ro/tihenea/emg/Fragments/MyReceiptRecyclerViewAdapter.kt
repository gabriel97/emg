package ro.tihenea.emg.Fragments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ro.tihenea.emg.R


import ro.tihenea.emg.Fragments.ReceiptFragment.OnListFragmentInteractionListener

import kotlinx.android.synthetic.main.fragment_receipt.view.*
import ro.tihenea.emg.model.Product

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class MyReceiptRecyclerViewAdapter(
    private val mValues: List<Product>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<MyReceiptRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Product
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_receipt, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mIdView.text = position.toString()
        holder.mProductName.text = item.produs
        holder.mproductPrice.text = item.pret.toString() + "Lei = "
        holder.noUnit.text = item.cantitate.toString()
        holder.productPriceTotal.text = item.categorie

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIdView: TextView = mView.item_number
        val mProductName: TextView = mView.productName
        val mproductPrice: TextView = mView.productPrice
        val productUM: TextView = mView.productUM
        val productPriceTotal: TextView = mView.productPriceTotal
        val noUnit: TextView = mView.noUnit

        override fun toString(): String {
            return super.toString() + " '" + mProductName.text + "'"
        }
    }
}
