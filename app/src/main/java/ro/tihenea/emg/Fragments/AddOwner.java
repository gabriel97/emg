package ro.tihenea.emg.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import ro.tihenea.emg.Constants;
import ro.tihenea.emg.R;
import ro.tihenea.emg.model.User;
import ro.tihenea.emg.service.DataService;

public class AddOwner extends AppCompatActivity {

    private AddOwner THIS;
    private DatabaseReference mUserDatabase;
    private EditText mSearchField;
    private RecyclerView mResultList;
    private Button mSearchBtn;
    private String accountId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_owner);

        THIS = this;
        Intent i = getIntent();
        accountId = i.getStringExtra(Constants.ACCOUNT_ID);
        mUserDatabase = FirebaseDatabase.getInstance().getReference("emg").child("users");


        mSearchField = (EditText) findViewById(R.id.search_field);
        mSearchBtn = (Button) findViewById(R.id.search_btn);

        mResultList = (RecyclerView) findViewById(R.id.result_list);
        mResultList.setHasFixedSize(true);
        mResultList.setLayoutManager(new LinearLayoutManager(this));

        mSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String searchText = mSearchField.getText().toString();

                firebaseUserSearch(searchText);

            }
        });
    }

    private void firebaseUserSearch(String searchText) {

        System.out.println(searchText);
        Query firebaseSearchQuery = mUserDatabase.orderByChild("name").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerAdapter<User, UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<User, UsersViewHolder>(

                User.class,
                R.layout.list_layout,
                UsersViewHolder.class,
                firebaseSearchQuery

        ) {
            @Override
            public void onBindViewHolder(UsersViewHolder viewHolder, int position) {
                super.onBindViewHolder(viewHolder, position);
//                holder.mItem = mValues.get(position);
//                holder.mIdView.setText(mValues.get(position).getName());
//                holder.mContentView.setText(mValues.get(position).getId());
                String name = viewHolder.userName;
                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //
                        System.out.println(viewHolder.userId + "---" + accountId);
                        DataService.getDataService().addUserToAccount(viewHolder.userId, accountId);
                        System.out.println(name);
                        finish();
                    }
                });
            }

            @Override
            protected void populateViewHolder(UsersViewHolder viewHolder, User model, int position) {

                System.out.println(model.getEmail());
                viewHolder.setDetails(getApplicationContext(), model.getName(), model.getId(), model.getName());

            }
        };

        mResultList.setAdapter(firebaseRecyclerAdapter);

    }


    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        public String userName;
        public String userId;

        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDetails(Context ctx, String userName, String userId, String userImage) {
            this.userName = userName;
            this.userId = userId;
            TextView user_name = (TextView) mView.findViewById(R.id.name_text);
//            TextView user_status = (TextView) mView.findViewById(R.id.status_text);
//            ImageView user_image = (ImageView) mView.findViewById(R.id.profile_image);


            user_name.setText(userName);
//            user_status.setText(userStatus);

        }


    }

}