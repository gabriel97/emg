package ro.tihenea.emg.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ro.tihenea.emg.R;
import ro.tihenea.emg.model.Account;
import ro.tihenea.emg.model.Category;
import ro.tihenea.emg.model.ProductDB;
import ro.tihenea.emg.service.DataService;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class AddProduct extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static String APIKEY = "nDDsJ7pFywR5Sit2K5ykmcjb9p4MdUF0VYS6p156C3Navx4429YLQbJW55YXk5y2";
    String currentPhotoPath;
    private AddProduct THIS;
    private Uri photoURI;
    private File photoFile;
    private Account account;
    private Category category;

    public static void addData(String token) {
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String endpoint = "https://api.tabscanner.com/api";
        try {
//            Toast.makeText(this,"OCR finished",Toast.LENGTH_LONG).show();
            JSONArray json = Unirest.get(endpoint + "/result/{token}")
                    .routeParam("token", token)
                    .header("apikey", APIKEY).asJson().getBody().getObject().getJSONObject("result").getJSONArray("lineItems");
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("produse");

            for (int i = 0; i < json.length(); i++) {
                JSONObject produs = json.getJSONObject(i);
                myRef.push().setValue(new ProductDB("Mancare", produs.getString("desc"), (float) produs.getDouble("lineTotal"), System.currentTimeMillis(), 1));
            }
//
        } catch (UnirestException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        System.out.println(DataService.getDataService().getAccounts());
        Spinner selectAccount = findViewById(R.id.selectAccount);
        String[] items = DataService.getDataService().getAccounts().stream().map(x -> x.getName()).collect(Collectors.toList()).toArray(new String[0]);
        ArrayAdapter<String> accountAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        selectAccount.setAdapter(accountAdapter);
        Spinner selectCategory = findViewById(R.id.selectCategory);


        selectAccount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Optional<Account> optionalAccount = DataService.getDataService().getAccounts().stream().filter(x -> x.getName().equals(adapterView.getAdapter().getItem(i))).findFirst();

                if (optionalAccount.isPresent()) {
                    account = optionalAccount.get();

                    String[] items2 = account.getCategories().stream().map(Category::getName).toArray(String[]::new);
                    ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_spinner_dropdown_item, items2);
                    selectCategory.setAdapter(categoryAdapter);
                    System.out.println();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        selectCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (account != null) {
                    Optional<Category> optionalCategory = account.getCategories().stream().filter(x -> x.getName().equals(adapterView.getAdapter().getItem(i))).findFirst();
                 optionalCategory.ifPresent(category1 -> category = category1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        THIS = this;
        Button save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                if (account != null && category != null) {
                    DatabaseReference myRef = database.getReference("emg").child("expenses").child(account.getId());
                    String produs = ((EditText) findViewById(R.id.produs)).getText().toString();
                    int cantiate = Integer.parseInt(((EditText) findViewById(R.id.cantitate)).getText().toString());
                    float pret = Float.parseFloat(((EditText) findViewById(R.id.pret)).getText().toString());
                    myRef.push().setValue(new ProductDB(category.getId(), produs, pret, System.currentTimeMillis() / 1000, cantiate));
                    THIS.finish();
                }
            }
        });
        Button poza = findViewById(R.id.scan);
        poza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent();

            }
        });
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date(System.currentTimeMillis()));
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_CANCELED) {
        } else if (requestCode == REQUEST_IMAGE_CAPTURE) {
            sendToApi();
            THIS.finish();
        }
    }

    private void sendToApi() {
        CompletableFuture<Void> jsonResponse = new CompletableFuture<>().supplyAsync(this::getData).thenAcceptAsync((x) -> {
            try {
                String token = x.getBody().getObject().get("token").toString();
                System.out.println(token);
                addData(token);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        while (!jsonResponse.isDone()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private HttpResponse<JsonNode> getData() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            System.out.println("sended");
            return Unirest.post("https://api.tabscanner.com/api/2/process")
                    .header("accept", "application/json")
                    .header("apikey", APIKEY)
                    .field("receiptImage", photoFile)
                    .asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }
}