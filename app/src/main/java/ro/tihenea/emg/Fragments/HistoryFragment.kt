package ro.tihenea.emg.Fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import ro.tihenea.emg.MainActivity
import ro.tihenea.emg.R
import ro.tihenea.emg.model.Product
import ro.tihenea.emg.model.Receipt
import ro.tihenea.emg.service.DataService
import ro.tihenea.emg.service.ProductCallBack

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [HistoryFragment.OnListFragmentInteractionListener] interface.
 */
class HistoryFragment : Fragment() {

    private lateinit var googleSignInAccount: GoogleSignInAccount
    private var columnCount = 1

    private var listener: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bigView = inflater.inflate(R.layout.fragment_history_list, container, false)
        val view = bigView.findViewById<RecyclerView>(R.id.list)
        val fab: View = bigView.findViewById(R.id.fab)
        fab.setOnClickListener {
            val i = Intent(context, AddProduct::class.java)
            startActivity(i)
        }
        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                val asd = object : ProductCallBack {
                    override fun renderProducts(ITEMS: MutableList<Product>) {
                        adapter = MyHistoryRecyclerViewAdapter(ITEMS, listener)
                    }

                }
                MainActivity.dataService.addProductListener(asd);
            }
        }
        return bigView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    fun setGoogle(googleSignInAccount: GoogleSignInAccount) {
        this.googleSignInAccount = googleSignInAccount
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: Receipt?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            HistoryFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}

data class Receipts(val test: List<Receipt>)
