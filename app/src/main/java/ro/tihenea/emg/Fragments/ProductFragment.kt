package ro.tihenea.emg.Fragments

import `in`.goodiebag.carouselpicker.CarouselPicker
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import ro.tihenea.emg.Constants
import ro.tihenea.emg.Fragments.dummy.DummyContent.DummyItem
import ro.tihenea.emg.R
import ro.tihenea.emg.model.ProductHistory
import java.io.UnsupportedEncodingException

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ProductFragment.OnListFragmentInteractionListener] interface.
 */
class ProductFragment : Fragment() {

    // TODO: Customize parameters
    private var columnCount = 1
    private var id: Long? = 0
    private var listener: OnListFragmentInteractionListener? = null
    private lateinit var googleSignInAccount: GoogleSignInAccount

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val bigView = inflater.inflate(R.layout.fragment_product_list, container, false)
        val view = bigView.findViewById<RecyclerView>(R.id.list)

        val carouselPicker = bigView.findViewById<CarouselPicker>(R.id.datePick)

        val imageItems: MutableList<CarouselPicker.PickerItem> = ArrayList();
        imageItems.add(CarouselPicker.DrawableItem(R.drawable.total))
        imageItems.add(CarouselPicker.DrawableItem(R.drawable.august))
        imageItems.add(CarouselPicker.DrawableItem(R.drawable.september))

        val imageAdapter: CarouselPicker.CarouselViewAdapter =
            CarouselPicker.CarouselViewAdapter(context, imageItems, 0);
        carouselPicker.setAdapter(imageAdapter);


        val mOnClickListener: ViewPager.OnPageChangeListener

        mOnClickListener = object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
//                Log.i("ageScro", " " + p0 + " " + p1 + " " + p2)
            }

            override fun onPageSelected(p0: Int) {
                Log.i("onPageSelected", " " + p0)

                // Set the adapter
                if (view is RecyclerView) {
                    with(view) {
                        layoutManager = when {
                            columnCount <= 1 -> LinearLayoutManager(context)
                            else -> GridLayoutManager(context, columnCount)
                        }
                        val queue = Volley.newRequestQueue(view.context)

                        val ITEMS: MutableList<ProductHistory> = ArrayList()
                        try {
                            Log.i("link", Constants.URL + Constants.RECEIPTS)
                            val stringRequest = StringRequest(
                                Request.Method.GET,
                                Constants.URL + Constants.PRODUCTS + "?token=" + googleSignInAccount.idToken + "&month=" + p0,
                                Response.Listener<String> { response ->
                                    Log.i("get", "{\"test\": $response}")
                                    val turnsType = object : TypeToken<List<ProductHistory>>() {}.type
                                    val recipits =
                                        Gson().fromJson("{\"test\": $response}", Products::class.java)
                                    ITEMS.addAll(recipits.test)
                                    adapter = MyProductRecyclerViewAdapter(ITEMS, listener)
                                    Log.i("itemsi", ITEMS.size.toString())
                                },
                                Response.ErrorListener { error ->
                                    Toast.makeText(view.context, error.toString(), Toast.LENGTH_SHORT).show()
                                })
                            queue.add(stringRequest)
                        } catch (e: JSONException) {
                        } catch (e: UnsupportedEncodingException) {
                        }
                    }
                }

            }

            override fun onPageScrollStateChanged(p0: Int) {
//                Log.i("onPageScrollStatChanged", " " + p0)

            }

        };

        carouselPicker.addOnPageChangeListener(mOnClickListener)

        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                val queue = Volley.newRequestQueue(view.context)

                val ITEMS: MutableList<ProductHistory> = ArrayList()
                try {
                    Log.i("link", Constants.URL + Constants.RECEIPTS)
                    val stringRequest = StringRequest(
                        Request.Method.GET,
                        Constants.URL + Constants.PRODUCTS + "?token=" + googleSignInAccount.idToken,
                        Response.Listener<String> { response ->
                            Log.i("get", "{\"test\": $response}")
                            val turnsType = object : TypeToken<List<ProductHistory>>() {}.type
                            val recipits =
                                Gson().fromJson("{\"test\": $response}", Products::class.java)
                            ITEMS.addAll(recipits.test)
                            ITEMS.add(ProductHistory("Coca Cola", "L", 300.0, 100.0))
                            adapter = MyProductRecyclerViewAdapter(ITEMS, listener)
                            Log.i("itemsi", ITEMS.size.toString())
                        },
                        Response.ErrorListener { error ->
                            Toast.makeText(view.context, error.toString(), Toast.LENGTH_SHORT).show()
                        })
                    queue.add(stringRequest)
                } catch (e: JSONException) {
                } catch (e: UnsupportedEncodingException) {
                }
            }
        }
        return bigView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    fun setProductId(id: Long?) {
        this.id = id
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: DummyItem?)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            ProductFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }

    fun setGoogle(googleSignInAccount: GoogleSignInAccount) {
        this.googleSignInAccount = googleSignInAccount
    }

}

data class Products(
    val test: List<ProductHistory>
)
