package ro.tihenea.emg.Fragments


import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_history.view.*
import ro.tihenea.emg.Fragments.HistoryFragment.OnListFragmentInteractionListener
import ro.tihenea.emg.model.Product
import ro.tihenea.emg.model.Receipt
import ro.tihenea.emg.service.DataService
import java.sql.Date
import java.sql.Timestamp


class MyHistoryRecyclerViewAdapter(
    private val mValues: List<Product>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<MyHistoryRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Receipt
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(ro.tihenea.emg.R.layout.fragment_history, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]

        holder.mCategory.text = DataService.getDataService().getCategoryName(item.account,item.categorie)
        holder.mIdView.text = item.produs
        val stamp = Timestamp(item.timestamp)
        holder.mContentView.text = ""//Date(stamp.getTime()).toString()
        holder.mContentPrice.text = (item.pret * item.cantitate).toString() + " Lei"
        Log.i("store", "item.store")
        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }
    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mCategory: TextView = mView.categorie
        val mIdView: TextView = mView.produs
        val mContentView: TextView = mView.addedAt
        val mContentPrice: TextView = mView.totalPrice

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
