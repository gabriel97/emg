package ro.tihenea.emg.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import ro.tihenea.emg.MainActivity;
import ro.tihenea.emg.R;
import ro.tihenea.emg.model.Product;
import ro.tihenea.emg.service.ProductCallBack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.github.mikephil.charting.utils.ColorTemplate.rgb;


public class PieFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    int cats[] = {
            R.id.cat1,
            R.id.cat2,
            R.id.cat3,
            R.id.cat4,
            R.id.cat5,
            R.id.cat6,
            R.id.cat7,
            R.id.cat8,
            R.id.cat9,
            R.id.cat10,
            R.id.cat11,
            R.id.cat12,
            R.id.cat13,
            R.id.cat14,
            R.id.cat15,
            R.id.cat16};
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View view;

    public PieFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PieFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PieFragment newInstance(String param1, String param2) {
        PieFragment fragment = new PieFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pie, container, false);
        hideAll(view);
        PieChart chart = view.findViewById(R.id.chart);

        int[] MATERIAL_COLORS = {
                rgb(getResources().getString(0+R.color.color1)),
                rgb(getResources().getString(0+R.color.color2)),
                rgb(getResources().getString(0+R.color.color3)),
                rgb(getResources().getString(0+R.color.color4)),
                rgb(getResources().getString(0+R.color.color5)),
                rgb(getResources().getString(0+R.color.color6)),
                rgb(getResources().getString(0+R.color.color7)),
                rgb(getResources().getString(0+R.color.color8)),
                rgb(getResources().getString(0+R.color.color9)),
                rgb(getResources().getString(0+R.color.color10)),
                rgb(getResources().getString(0+R.color.color11)),
                rgb(getResources().getString(0+R.color.color12)),
                rgb(getResources().getString(0+R.color.color13)),
                rgb(getResources().getString(0+R.color.color14)),
                rgb(getResources().getString(0+R.color.color15)),
                rgb(getResources().getString(0+R.color.color16))
        };


        ProductCallBack asd = products -> {
            List<PieEntry> entries = new ArrayList<PieEntry>();
            PieDataSet dataSet = new PieDataSet(entries, "Label");
            Map<String, List<Product>> categories = products.stream().collect(Collectors.groupingBy(Product::getCategorie));
            hideAll(view);
            unhideCat(categories.size());
            categories.entrySet().forEach((entry) -> {

                float categoryTotal = entry.getValue().stream().map(x -> x.getPret() * x.getCantitate()).reduce(0f, (x, y) -> x + y);
                entries.add(new PieEntry(categoryTotal));
            });
            dataSet.setColor(Color.YELLOW);
            dataSet.setSliceSpace(3f);
//        dataSet.setSelectionShift(5f);

            //dataSet.setColors(populateColors());// add a lot of colors
            dataSet.setColors(MATERIAL_COLORS);
//        dataSet.setValueLinePart1OffsetPercentage(80.f);
//        dataSet.setValueLinePart1Length(0.2f);
//        dataSet.setValueLinePart2Length(0.4f);
            // dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
//        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
            dataSet.setValueTextSize(11f);
            dataSet.setSliceSpace(5f);
            PieData lineData = new PieData(dataSet);
            chart.setHoleRadius(75);
            chart.getLegend().setEnabled(false);
            chart.setContentDescription("");
            chart.getDescription().setText("");
            chart.setData(lineData);
            chart.invalidate(); // refresh
        };
        MainActivity.dataService.addProductListener(asd);
        return view;
    }

    private void unhideCat(int size) {
        for (int i = 0; i < size; i++) {
            view.findViewById(cats[i]).setVisibility(View.VISIBLE);
        }
    }

    private void hideAll(View view) {
        for (int cat : cats) {
            view.findViewById(cat).setVisibility(View.INVISIBLE);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
