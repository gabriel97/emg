package ro.tihenea.emg.Fragments


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_buylist.view.*
import ro.tihenea.emg.Fragments.BuyListFragment.OnListFragmentInteractionListener
import ro.tihenea.emg.R
import ro.tihenea.emg.model.ListContent.ListItem

/**
 * [RecyclerView.Adapter] that can display a [ListItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class MyBuyListRecyclerViewAdapter(
    private val mValues: List<ListItem>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<MyBuyListRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ListItem
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_buylist, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mIdView.text = item.store
        holder.mContentView.text = item.store

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIdView: TextView = mView.item_number
        val mContentView: TextView = mView.receiptProduct

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
