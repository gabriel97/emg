package ro.tihenea.emg;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.*;
import com.google.firebase.iid.FirebaseInstanceId;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ro.tihenea.emg.Fragments.*;
import ro.tihenea.emg.Fragments.dummy.DummyContent;
import ro.tihenea.emg.model.*;
import ro.tihenea.emg.service.DataService;
import ro.tihenea.emg.service.TimeInterval;
import ro.tihenea.emg.service.TimeIntervalCallBack;

import java.util.*;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
        , BuyListFragment.OnListFragmentInteractionListener,
        HistoryFragment.OnListFragmentInteractionListener,
        ReceiptFragment.OnListFragmentInteractionListener,
        ProductFragment.OnListFragmentInteractionListener,
        TimeIntervalCallBack {
    public static DataService dataService = DataService.getDataService();
    private static User user;
    TimeInterval timeInterval;
    PieFragment pieFragment = new PieFragment();
    HistoryFragment history = new HistoryFragment();
    private final BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = (BottomNavigationView.OnNavigationItemSelectedListener) (new BottomNavigationView.OnNavigationItemSelectedListener() {
        public final boolean onNavigationItemSelected(@NotNull MenuItem item) {
            Intrinsics.checkParameterIsNotNull(item, "item");
            FragmentTransaction var10000;
            FragmentTransaction ft;
            switch (item.getItemId()) {
                case R.id.navigation_history:
                    ft = MainActivity.this.getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.content_frame, (Fragment) history);
                    ft.commit();
                    return true;
                case R.id.navigation_scan:
                    ft = MainActivity.this.getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.content_frame, (Fragment) pieFragment);
                    ft.commit();
                    return true;
                default:


                    return false;
               /* case R.id.navigation_sugestions:
                    var10000 = MainActivity.this.getSupportFragmentManager().beginTransaction();
                    AccountsFragment accountsActivity = new AccountsFragment();
                    var10000.replace(R.id.content_frame, (Fragment) accountsActivity);
                    var10000.commit();
                    return true;
                case R.id.navigation_buy_list:
                    var10000 = MainActivity.this.getSupportFragmentManager().beginTransaction();
                    Intrinsics.checkExpressionValueIsNotNull(var10000, "supportFragmentManager.beginTransaction()");
                    ft = var10000;
                    ft.replace(R.id.content_frame, (Fragment) (new BuyListFragment()));
                    ft.commit();
                    return true;*/
            }
        }
    });

    public static User getUser() {
        return user;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        timeInterval = new TimeInterval(this);
        timeInterval.registerCallback(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        View var10000 = this.findViewById(R.id.bottom_nav_view);
        Intrinsics.checkExpressionValueIsNotNull(var10000, "findViewById(R.id.bottom_nav_view)");
        BottomNavigationView navView = (BottomNavigationView) var10000;
        navView.setOnNavigationItemSelectedListener(this.onNavigationItemSelectedListener);
        FragmentTransaction var5 = this.getSupportFragmentManager().beginTransaction();
        Intrinsics.checkExpressionValueIsNotNull(var5, "supportFragmentManager.beginTransaction()");
        FragmentTransaction ft = var5;
        HistoryFragment history = new HistoryFragment();
        ft.replace(R.id.content_frame, (Fragment) history);
        ft.commit();
        init();

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();
            Log.e("newToken", newToken);
           this.getPreferences(Context.MODE_PRIVATE).edit().putString("fb", newToken).apply();
        });

        Log.d("newToken", this.getPreferences(Context.MODE_PRIVATE).getString("fb", "empty :("));

    }

    private void init() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("emg");
        String uid = LoginActivity.getUser().getUid();
        myRef.child("users").child(uid).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    user = dataSnapshot.getValue(User.class);
                    DataService.getDataService().initData(user);
                    System.out.println("Already exists");
                } else {
                    user = new ro.tihenea.emg.model.User();
                    user.setEmail(LoginActivity.getUser().getEmail());
                    user.setName(LoginActivity.getUser().getDisplayName());
                    Account account = new Account();
                    account.setName("Test");
                    HashMap<String, Boolean> owners = new HashMap<>();
                    owners.put(uid, true);
                    account.setOwners(owners);
                    account.setCategories(new LinkedList<>());
                    account.getCategories().add(new Category("1234", "Mancare"));
                    DatabaseReference newAccount = myRef.child("accounts").push();
                    String accountId = newAccount.getKey();
                    newAccount.setValue(account);
                    user.setAccounts(new HashMap<>());
                    user.getAccounts().put(accountId, true);
                    myRef.child("users").child(uid).setValue(user);
//                    myRef.child("usersList").child(Objects.requireNonNull(LoginActivity.getUser().getEmail()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("rsf", "loadPost:onCancelled", databaseError.toException());

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        initTimeMenu();

        return true;
    }

    private void initTimeMenu() {

        TextView timeLeft = findViewById(R.id.toolbar_left);
        timeLeft.setOnClickListener(v -> timeInterval.previousTimeInterval());
        TextView timeSelect = findViewById(R.id.toolbar_center);
        timeInterval.selectText(timeSelect);
        timeSelect.setOnClickListener(v -> timeInterval.showTimeSelect(getFragmentManager()));
        TextView timeRight = findViewById(R.id.toolbar_right);
        timeRight.setOnClickListener(v -> timeInterval.nextTimeInterval());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }
*/
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_tools) {
            Intent intent = new Intent(this, AccountsActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onListFragmentInteraction(@Nullable DummyContent.DummyItem item) {

    }

    public void onListFragmentInteraction(@NotNull Product item) {

    }

    public void onListFragmentInteraction(@Nullable ListContent.ListItem item) {

    }

    public void onListFragmentInteraction(@Nullable Receipt item) {
        ActionBar actionBar = this.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        FragmentTransaction var4 = this.getSupportFragmentManager().beginTransaction();
        Intrinsics.checkExpressionValueIsNotNull(var4, "supportFragmentManager.beginTransaction()");
        FragmentTransaction ft = var4;
        ReceiptFragment receiptFragment = new ReceiptFragment();
        receiptFragment.setReceipt(item);
        ft.replace(R.id.content_frame, (Fragment) receiptFragment);
        ft.commit();
    }

    @Override
    public void onTimeIntervalChange(Calendar start, Calendar end) {
        dataService.setTimeInterval(start, end);
    }
}
